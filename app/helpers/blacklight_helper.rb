# frozen_string_literal: true

module BlacklightHelper
  include Blacklight::BlacklightHelperBehavior

  def application_name
    'NLI Web Archive Portal'
  end
end
