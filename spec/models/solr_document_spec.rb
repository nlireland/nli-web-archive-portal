# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SolrDocument, type: :model do
  let(:document) { SolrDocument.new }

  describe '#replay_link' do
    let(:document) do
      SolrDocument.new(wayback_date: '20071204055200', url: 'http://www.dublinfirebrigade.ie/Images/gaeilge/Vol%203%20-%20Figure%204%20River%20Basin%20Districts_tcm36-12715.pdf')
    end

    it 'writes a replay url based on memento time travel response' do
      expect(document.replay_link).to eq '<a href="http://webarchiveportal.nli.ie:8080/all/20071204055200/http://www.dublinfirebrigade.ie/Ima...rigade.ie/Images/gaeilge/Vol%203%20-%20Figure%204%20River%20Basin%20Districts_tcm36-12715.pdf"</a>'
    end

    context 'when time_travel_response.empty?' do
      let(:document) do
        SolrDocument.new(wayback_date: '20150113163601', url: 'http://www.library.yorku.ca/cms/steacie/about-the-library/hackfest/FAIL')
      end

      it 'writes a replay url that is "Not Available."' do
        expect(document.replay_link).to eq 'Not Available.'
      end
    end
  end
end
