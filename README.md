# NLI Web Archive App

Rails App to Browse and Search Web Archive Domain Crawls


* Ruby version
2.6.3

* System dependencies

- Rails 5.1.7
- Solr 7
- PYWB - [PYWB](https://pywb.readthedocs.io/en/latest/)

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
